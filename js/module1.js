export class Coins {
    #coinsData = []

    constructor(className){
       
        this.create(className)
        this.getCoinsData()
       

   
    }

    create(className){
        const root = document.querySelector(`${className}`)
        if(!root){
            return;
        }
        const table = this.createHtml()
        root.appendChild(table)
        this. renderCoins()
    }

    createHtml(){
        const coinsTable = document.createElement('div')
        coinsTable.classList.add('coins__table')
        coinsTable.innerHTML = ` <div class= "coins__table__header">
                              
                                </div>

                                <div class= "coins__body"></div>
        
        
        `
        return coinsTable
    }

    async  getCoinsData(){

    
        const response = await fetch('https://jsonplaceholder.typicode.com/users')
        const dataJason = await response.json();
        this.#coinsData = [...dataJason]
        this.renderCoins()
    }


    renderCoins(){
        const coinsBody = document.querySelector('.coins__body')
        if(this.#coinsData.length === 0){
            coinsBody.innerHTML = `<div class="preolder">
            <span class="loader">L &nbsp; ading</span>
            
            </div>`
            return
        }

        let coinsList = ' ';
        this.#coinsData.forEach(({name,phone,address,email,id})=>{
            coinsList += `<div class = "coin__item">
            
            <li>
            <div class="contact__name">Имя: ${name}</div>
            <div class="contact__phone">Телефон: ${phone}</div>
            <div class="address">Адрес: ${address.city}</div>
            <div class="contact__email">Email: ${email}</div>
            <button class="delete__button" id="${id}">Удалить</button>
        </li>
            
            </div>`
        }) 
        coinsBody.innerHTML = coinsList
    }
   

    }
const coins = new Coins('.app')
console.log(coins);